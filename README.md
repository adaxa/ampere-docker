# Ampere Docker Compose configuration example

Basic docker setup for [Ampere](https://gitlab.com/adaxa/ampere).

## Getting Started

Introduction deploying (with Docker compose).

### Prerequisites

You need git and docker-compose > 1.20 (tested with version 1.26.2) installed on your system.
OpenSSL (version >= 1.1.1) is also required if you want to generate self-signed certificates. Currently compiles against openjdk-8.

Instructions and scripts assume Ubuntu 18.04 as the OS. 

Some guidance on setting pre-requisites up can be found at https://gitlab.com/adaxa/ampere/-/wikis/Deployment-guide

### Installing

Clone this repository.

```
git clone https://gitlab.com/adaxa/ampere-docker.git
cd ampere-docker
```

Generate the config file (creates random passwords and sets hostname and domain). Optionally use env variables to configure:  AMPERE_HOSTNAME,... see setup.sh for available options.

```
AMPERE_HOSTNAME=ampere AMPERE_DOMAIN=ampere.local ./setup.sh
```

Customise the generated .env file e.g. to change passwords
```
vi .env
```
Optionally create self-signed certificates for testing on system without access for letsencrypt (NB script uses sudo to update host certificate store which you may not want to do!)

```
sudo apt install ca-certificates-java
cd certs
./generate_ssl_cert_and_truststore
cd ..
```
If you host is not accessible via DNS, add rule to hosts for your selected hostname

```
sudo -- sh -c "echo 127.0.0.1 ampere.ampere.local >> /etc/hosts"

```

Fix permissions for letsencrypt acme.json.
Create a docker network, then fire up the docker containers


```
chmod 600 traefik/acme.json
docker network create web
docker compose build
```
Start the postgres container separately the first time to initialise the database

```
docker compose up postgres
```

Ctrl-C to kill when postgres reports it is ready and accepting connections.

Start all the containers

```
docker compose up -d
```

Open https://ampere.ampere.local in your browser to view landing page.

You will need to [manually setup users in keycloak](https://gitlab.com/adaxa/ampere/wikis/Add-users-to-keycloak) to be able to access the applications:



## License

This project is licensed under the unlicense
