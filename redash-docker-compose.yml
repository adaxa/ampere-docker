version: '3.4'

x-redash-environment:
  &rd-env
    PYTHONUNBUFFERED: 0
    REDASH_LOG_LEVEL: INFO
    REDASH_REDIS_URL: redis://redis:6379/0
    REDASH_COOKIE_SECRET: ${REDASH_COOKIE_SECRET}
    REDASH_SECRET_KEY: ${REDASH_SECRET_KEY}
    REDASH_DATABASE_URL: ${REDASH_DATABASE_URL}
    REDASH_JWT_LOGIN_ENABLED: "true"
    REDASH_JWT_AUTH_ISSUER: ${REDASH_JWT_AUTH_ISSUER}
    REDASH_JWT_AUTH_PUBLIC_CERTS_URL: ${REDASH_JWT_AUTH_PUBLIC_CERTS_URL}
    REDASH_JWT_AUTH_AUDIENCE: redash
    REDASH_JWT_AUTH_HEADER_NAME: X-Auth-Token

x-redash-service: &redash-service
  image: redash/redash:8.0.2.b37747
  depends_on:
    - rd-postgres
    - redis
  restart: always

services:
  gatekeeper:
    image: keycloak/keycloak-gatekeeper:5.0.0
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.gatekeeper.rule=Host(`${REDASH_HOSTNAME}.${DOMAIN}`)"
      - "traefik.http.routers.gatekeeper.entrypoints=websecure"
      - "traefik.http.routers.gatekeeper.tls.certresolver=le"
      - "traefik.http.services.gatekeeper.loadbalancer.server.port=3000"


    networks:
      - web
      - default
    volumes:
      - ./gatekeeper/keycloak-gatekeeper.conf:/etc/keycloak-gatekeeper.conf
    entrypoint:
      - /opt/keycloak-gatekeeper
      - --config=/etc/keycloak-gatekeeper.conf
      - --discovery-url=${GATEKEEPER_DISCOVERY_URL}
      - --client-secret=${GATEKEEPER_CLIENT_SECRET}
      - --redirection-url=${GATEKEEPER_REDIRECTION_URL}
      - --encryption-key=${GATEKEEPER_ENCRYPTION_KEY}
      - --upstream-url=http://redash-server:5000/

  redash-server:
    <<: *redash-service
    command: server
    environment:
      <<: *rd-env
      REDASH_WEB_WORKERS: 4
    labels:
      - "traefik.backend=redash-server"
    networks:
      - web
      - default

  redash-scheduler:
    <<: *redash-service
    command: scheduler
    environment:
      <<: *rd-env
      QUEUES: "celery"
      WORKERS_COUNT: 1

  redash-worker:
    <<: *redash-service
    command: worker
    environment:
      <<: *rd-env
      QUEUES: "queues,scheduled_queries,schemas"
      WORKERS_COUNT: 1

  redis:
    image: redis:5.0-alpine
    restart: always
    networks:
      - default

  rd-postgres:
    image: postgres:9.6-alpine
    environment:
      - POSTGRES_PASSWORD=${REDASH_POSTGRES_PASSWORD}
    volumes:
      - redash_pg_data:/var/lib/postgresql/data
    restart: always
    networks:
      - default

volumes:
  redash_pg_data: 
